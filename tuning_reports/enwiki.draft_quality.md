# Model tuning report
- Revscoring version: 2.0.3
- Features: draftquality.feature_lists.enwiki.draft_quality
- Date: 2017-08-31T17:23:51.043736
- Observations: 201261
- Labels: ["attack", "vandalism", "OK", "spam"]
- Statistic: roc_auc.macro (maximize)
- Folds: 5

# Top scoring configurations
| model   | roc_auc.macro   | params   |
||

# Models
